
USE snipcat_db;

SET NAMES UTF8;

SET @@foreign_key_checks = 0;
TRUNCATE TABLE category;
INSERT INTO category VALUES
	( 1, 'Strings', 'strings', NULL, 0 ),
	( 2, 'Regular expressions', 'regular-expressions', NULL, 0 ),
	( 3, 'Graphics and images', 'graphics-images', NULL, 0 ),
	( 4, 'Calendar and dates', 'calendar-dates', NULL, 0 );
SET @@foreign_key_checks = 1;

