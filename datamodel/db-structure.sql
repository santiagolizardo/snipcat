SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `snipcat_db` ;
CREATE SCHEMA IF NOT EXISTS `snipcat_db` DEFAULT CHARACTER SET utf8 ;
USE `snipcat_db` ;

-- -----------------------------------------------------
-- Table `snipcat_db`.`category`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`category` (
  `category_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `sanitized_name` VARCHAR(45) NOT NULL ,
  `description` MEDIUMTEXT NULL ,
  `num_snips` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`category_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`developer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`developer` (
  `developer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `reg_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `username` VARCHAR(45) NULL ,
  `email` VARCHAR(45) NULL ,
  `password` VARCHAR(45) NULL ,
  PRIMARY KEY (`developer_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`language`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`language` (
  `language_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `sanitized_name` VARCHAR(45) NOT NULL ,
  `description` MEDIUMTEXT NULL ,
  `num_snips` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`language_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`snip`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`snip` (
  `snip_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `creation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `language_id` INT UNSIGNED NOT NULL ,
  `category_id` INT UNSIGNED NOT NULL ,
  `developer_id` INT UNSIGNED NOT NULL ,
  `code` TEXT NOT NULL ,
  `hl_code` TEXT NULL ,
  `output` MEDIUMTEXT NULL ,
  `title` VARCHAR(145) NOT NULL ,
  `sanitized_title` VARCHAR(145) NOT NULL ,
  `description` MEDIUMTEXT NULL ,
  `tags` VARCHAR(145) NOT NULL ,
  `num_views` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `num_votes` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `num_lines` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `num_favs` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  `votes_values` SMALLINT NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`snip_id`) ,
  INDEX `fk_snip_category1` (`category_id` ASC) ,
  INDEX `fk_snip_developer1` (`developer_id` ASC) ,
  INDEX `fk_snip_language1` (`language_id` ASC) ,
  CONSTRAINT `fk_snip_category1`
    FOREIGN KEY (`category_id` )
    REFERENCES `snipcat_db`.`category` (`category_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_snip_developer1`
    FOREIGN KEY (`developer_id` )
    REFERENCES `snipcat_db`.`developer` (`developer_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_snip_language1`
    FOREIGN KEY (`language_id` )
    REFERENCES `snipcat_db`.`language` (`language_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`tag`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`tag` (
  `tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `num_snips` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`tag_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`comment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`comment` (
  `comment_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `creation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `snip_id` INT UNSIGNED NOT NULL ,
  `developer_id` INT UNSIGNED NOT NULL ,
  `body` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`comment_id`) ,
  INDEX `fk_comment_developer` (`developer_id` ASC) ,
  INDEX `fk_comment_snip1` (`snip_id` ASC) ,
  CONSTRAINT `fk_comment_developer`
    FOREIGN KEY (`developer_id` )
    REFERENCES `snipcat_db`.`developer` (`developer_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_snip1`
    FOREIGN KEY (`snip_id` )
    REFERENCES `snipcat_db`.`snip` (`snip_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`developer_url`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`developer_url` (
  `developer_id` INT UNSIGNED NOT NULL ,
  `ohloh` VARCHAR(145) NULL ,
  `sourceforge` VARCHAR(145) NULL ,
  `github` VARCHAR(145) NULL ,
  `google_code` VARCHAR(145) NULL ,
  `linkedin` VARCHAR(145) NULL ,
  `blog` VARCHAR(145) NULL ,
  `twitter` VARCHAR(145) NULL ,
  `codeplex` VARCHAR(145) NULL ,
  PRIMARY KEY (`developer_id`) ,
  CONSTRAINT `fk_developer_url_developer1`
    FOREIGN KEY (`developer_id` )
    REFERENCES `snipcat_db`.`developer` (`developer_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snipcat_db`.`developer_fav`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `snipcat_db`.`developer_fav` (
  `developer_id` INT UNSIGNED NOT NULL ,
  `snip_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`developer_id`, `snip_id`) ,
  INDEX `fk_developer_fav_snip1` (`snip_id` ASC) ,
  CONSTRAINT `fk_developer_fav_developer1`
    FOREIGN KEY (`developer_id` )
    REFERENCES `snipcat_db`.`developer` (`developer_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_developer_fav_snip1`
    FOREIGN KEY (`snip_id` )
    REFERENCES `snipcat_db`.`snip` (`snip_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
