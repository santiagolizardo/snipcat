
import webapp2
import string

baseChars = list(  string.digits + string.letters + '_-' )
base = len( baseChars )

def convertToBaseX( number ):
	string = ''
	total = number
	while total:
		remainder = total % base
		string = baseChars[ remainder ] + string
		quotient = total // base
		total = quotient
	return string

def convertFromBaseX( string ):
	number = 0
	stringLen = len( string )
	i = stringLen - 1
	while i > -1:
		pos = baseChars.index( string[i-1] )
		plus = pos * ( 64 ** i )
		number += plus 
		i -= 1
	return number
		
def buildUrl( name, **kwargs ):
	app = webapp2.get_app()
	appDomain = app.config.get( 'appDomain' )
	netloc = 'www'
	if name[0:2] == 'go': netloc = 'go'
	elif name[0:5] == 'embed': netloc = 'embed'
	elif name[0:4] == 'code': netloc = 'code'
	elif name[0:3] == 'dev': netloc = 'developer'
	elif name[0:6] == 'search': netloc = 'search'
	elif name[0:7] == 'snippet': netloc = 'snippet'
	elif name[0:8] == 'snip-add': netloc = 'www'
	elif name[0:4] == 'snip': netloc = 'code'
	kwargs['_netloc'] = netloc + appDomain
	url = webapp2.uri_for( name ,**kwargs )
	return url


