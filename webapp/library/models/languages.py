
from library.db import connectDb, convertToDict

def findLanguageBySanitizedName( sanitizedName ):
	cols = [ 'language_id', 'name', 'sanitized_name', 'description' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = 'SELECT %s FROM language WHERE sanitized_name = %%s' % ', '.join( cols )
	cursor.execute( sql, sanitizedName )
	language = cursor.fetchone()
	conn.close()
	if language is None:
		return None
	return dict( zip( cols, language ) )

def listLanguages():
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( 'SELECT language_id, name, sanitized_name FROM language' )
	languages = cursor.fetchall()
	conn.close()
	return convertToDict( languages, [ 'language_id','name','sanitized_name' ] ) 

def listPopularLanguages():
	cols = [ 'name', 'sanitized_name', 'num_snips' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		%s
	FROM
		language
	ORDER BY
		num_snips DESC
	LIMIT 10
	""" % ', '.join( cols )
	cursor.execute( sql )
	languages = cursor.fetchall()
	conn.close()
	return convertToDict( languages, cols ) 

