
from library.db import connectDb, convertToDict

def findDevelopersByLanguageId( languageId ):
	sql = '''
	SELECT
		d.username, COUNT(*) AS num_snips_per_developer
	FROM
		developer d
		INNER JOIN snip s USING (developer_id)
		INNER JOIN language l USING(language_id)
	WHERE
		l.language_id = %s
	GROUP BY
		d.username
	'''
	cols = [ 'username', 'num_snips_per_developer' ]
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( sql, languageId )
	developers = cursor.fetchall()
	conn.close()
	return convertToDict( developers, cols )	

def listDevelopers():
	cols = [ 'username' ]
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( 'SELECT %s FROM developer' % ( ', '.join( cols ) ) )
	developers = cursor.fetchall()
	conn.close()
	return convertToDict( developers, cols )

def listPopularDevelopers():
	cols = [ 'username', 'num_snips' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		d.username, ( SELECT COUNT( * ) FROM snip WHERE developer_id = d.developer_id ) AS num_snips
	FROM
		developer d
	"""
	cursor.execute( sql )
	developers = cursor.fetchall()
	conn.close()
	return convertToDict( developers, cols )

def findDeveloperByUsername( username ):
	cols = [ 'developer_id', 'reg_date', 'username' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = 'SELECT %s FROM developer WHERE username = %%s' % ', '.join( cols )
	cursor.execute( sql, username )
	developer = cursor.fetchone()
	conn.close()
	return dict( zip( cols, developer ) ) if developer else None

def findDeveloperByUsernamePassword( username, password ):
	cols = [ 'developer_id', 'username' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = '''
	SELECT
		%s
	FROM
		developer
	WHERE
		username = %%s
		AND password = %%s
	''' % ', '.join( cols )
	cursor.execute( sql, ( username, password ) )
	developer = cursor.fetchone()
	conn.close()
	if not developer:
		return None
	return dict( zip( cols, developer ) )

def updateDeveloperUrls( developerId, urls ):
	conn = connectDb()
	cursor = conn.cursor()
	sql = '''
	INSERT INTO
		developer_url
		( developer_id, ohloh, sourceforge, github, google_code, linkedin, blog, twitter, codeplex )
	VALUES
		( %s, %s, %s, %s, %s, %s, %s, %s, %s )
	ON DUPLICATE KEY
	UPDATE
		ohloh = %s,
		sourceforge = %s,
		github = %s,
		google_code = %s,
		linkedin = %s,
		blog = %s,
		twitter = %s,
		codeplex = %s
	'''
	urls['developer_id'] = developerId
	cursor.execute( sql,
		( developerId,
			urls['ohloh'], urls['sourceforge'], urls['github'], urls['google_code'], urls['linkedin'], urls['blog'], urls['twitter'], urls['codeplex'],
			urls['ohloh'], urls['sourceforge'], urls['github'], urls['google_code'], urls['linkedin'], urls['blog'], urls['twitter'], urls['codeplex']
		)
	)
	conn.commit()
	conn.close()


def findUrlsByDeveloperId( developerId ):
	cols = [ 'ohloh', 'sourceforge', 'github', 'google_code', 'linkedin', 'blog', 'twitter', 'codeplex' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = '''
	SELECT
		ohloh, sourceforge, github, google_code, linkedin, blog, twitter, codeplex
	FROM
		developer_url
	WHERE
		developer_id = %s
	'''
	cursor.execute( sql, developerId )
	urls = cursor.fetchone()
	conn.close()
	return dict( zip( cols, urls ) ) if urls else dict( zip( cols, [ '','','','','','','', '' ] ) )

def registerDeveloper( developer ):
	values = ( developer['username'], developer['email'], developer['password'] )
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( 'INSERT INTO developer ( username, email, password ) VALUES ( %s, %s, %s )', values )
	conn.commit()
	conn.close()

