
from google.appengine.ext import db

class Tag( db.Model ):
#	name = db.StringProperty( required = True )
	num_snips = db.IntegerProperty( required = True, default = 0 )
	last_modification_date = db.DateTimeProperty( auto_now = True )

def listPopularTags():
	return Tag.all().order( '-num_snips' ).fetch( 20 )

def listTags():
	return Tag.all()

