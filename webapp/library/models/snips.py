
from library.db import connectDb, convertToDict
from library.strings import sanitizeString

def increaseSnipViews( snipId ):
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	UPDATE
		snip
	SET
		num_views = num_views + 1
	WHERE
		snip_id = %s
	"""
	cursor.execute( sql, snipId )
	cursor.close()
	conn.commit()
	conn.close()

def castSnipVote( snipId, voteValue ):
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	UPDATE
		snip
	SET
		num_votes = num_votes + 1,
		votes_values = votes_values + %s
	WHERE
		snip_id = %s
	"""
	cursor.execute( sql, ( voteValue, snipId ) )
	cursor.close()
	conn.commit()
	conn.close()

def listSnips( languageId, categoryId ):
	cols = [ 'title', 'sanitized_title' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		%s	
	FROM
		snip
	WHERE
		language_id = %%s
		AND category_id = %%s
	""" % ', '.join( cols )
	cursor.execute( sql, ( languageId, categoryId ) )
	snips = cursor.fetchall()
	conn.close()
	return convertToDict( snips, cols )

def listPopularSnips():
	cols = [ 'title', 'sanitized_title', 'votes_values', 'lang', 'cat' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		s.title,
		s.sanitized_title,
		s.votes_values,
		l.sanitized_name,
		c.sanitized_name
	FROM
		snip s
		INNER JOIN language l USING ( language_id )
		INNER JOIN category c USING ( category_id )
	ORDER BY
		s.votes_values DESC
	LIMIT 10
	"""
	cursor.execute( sql )
	snips = cursor.fetchall()
	conn.close()
	return convertToDict( snips, cols )

def findSnipsByDeveloperId( developerId ):
	cols = [ 'creation_date', 'title', 'description', 'sanitized_title', 'lang', 'cat' ]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		s.creation_date,
		s.title,
		s.description,
		s.sanitized_title,
		l.sanitized_name,
		c.sanitized_name
	FROM
		snip s
		INNER JOIN language l USING ( language_id )
		INNER JOIN category c USING ( category_id )
	WHERE
		s.developer_id = %s
	ORDER BY
		s.creation_date DESC
	"""
	cursor.execute( sql, developerId )
	snips = cursor.fetchall()
	conn.close()
	return convertToDict( snips, cols )

def findSnipUrlById( snipId ):
	cols = [
		'language_name', 'category_name', 'sanitized_title'
	]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		l.sanitized_name,
		c.sanitized_name,
		s.sanitized_title
	FROM
		snip s
		INNER JOIN language l USING ( language_id )
		INNER JOIN category c USING ( category_id )
	WHERE
		snip_id = %s
	"""
	cursor.execute( sql, ( snipId ) )
	snip = cursor.fetchone()
	if snip:
		snip = dict( zip( cols, snip ) )
	conn.close()
	return snip

def findSnipBySanitizedTitle( title ):
	cols = [
		'snip_id', 'creation_date', 'title', 'description', 'code', 'output', 'tags', 'num_views', 'num_votes', 'votes_values',
		'num_lines', 'developer_username',
		'language_name', 'language_sanitized_name',
		'category_name', 'category_sanitized_name'
	]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		s.snip_id, s.creation_date, s.title, s.description, s.code, s.output, s.tags, s.num_views, s.num_votes, s.votes_values,
		s.num_lines, d.username,
		l.name, l.sanitized_name,
		c.name, c.sanitized_name
	FROM
		snip s
		INNER JOIN developer d USING ( developer_id )
		INNER JOIN language l USING ( language_id )
		INNER JOIN category c USING ( category_id )
	WHERE
		sanitized_title = %s
	"""
	cursor.execute( sql, ( title ) )
	snip = cursor.fetchone()
	if not snip:
		return None
	snip = dict( zip( cols, snip ) )
	snip['tags'] = snip['tags'].split( ',' ) if snip['tags'] != '' else []
	snip['code'] = snip['code'].replace( '<', '&lt;' )
	if snip['num_votes'] > 0:
		snip['votes_average'] = '%.2f' % ( float( snip['votes_values'] ) / float( snip['num_votes'] ) )
	else:
		snip['votes_average'] = '0'
	conn.close()
	return snip

def findSnipById( snipId ):
	cols = [
		'snip_id', 'creation_date', 'title', 'description', 'code', 'hl_code', 'tags', 'num_views', 'num_votes',
		'developer_username'
	]
	conn = connectDb()
	cursor = conn.cursor()
	sql = """
	SELECT
		s.snip_id, s.creation_date, s.title, s.description, s.code, s.hl_code, s.tags, s.num_views, s.num_votes,
		d.username
	FROM
		snip s
		INNER JOIN developer d USING ( developer_id )
	WHERE
		snip_id = %s
	"""
	cursor.execute( sql, ( snipId ) )
	snip = cursor.fetchone()
	snip = dict( zip( cols, snip ) )
	snip['tags'] = snip['tags'].split( ',' ) if snip['tags'] != '' else []
	snip['code'] = snip['code'].replace( '<', '&lt;' )
	conn.close()
	return snip

def addSnip( snip ):
	sanitizedTitle = sanitizeString( snip['title'] )
	numLines = snip['code'].count( '\n' )
	values = (
		snip['languageId'],
		snip['categoryId'],
		snip['developerId'],
		snip['code'],
		snip['output'],
		snip['title'],
		sanitizedTitle,
		snip['description'],
		snip['tags'],
		numLines
	)

	conn = connectDb()
	import logging, sys
	try:
		cursor = conn.cursor()
		cursor.execute( 'INSERT INTO snip ( language_id, category_id, developer_id, code, output, title, sanitized_title, description, tags, num_lines ) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )', values )
		cursor.close()

		cursor = conn.cursor()
		cursor.execute( 'UPDATE language SET num_snips = num_snips + 1 WHERE language_id = %s', snip['languageId'] )
		cursor.close()

		from library.models.tags import Tag
		tagsArray = snip['tags'].split( ',' )
		for tag in tagsArray:
			tagModel = Tag.get_or_insert( tag )
			tagModel.num_snips = tagModel.num_snips + 1
			tagModel.save()

		conn.commit()
	except:
		logging.error(sys.exc_info()[1])
		conn.rollback()
	conn.close()

