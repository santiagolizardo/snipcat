
from google.appengine.ext import db

class SearchQuery( db.Model ):
	search_date = db.DateTimeProperty( auto_now_add = True )
	string = db.StringProperty( required = True, multiline = False )
	sanitized_string = db.StringProperty( required = True, multiline = False )

def listSearchQueries():
	return SearchQuery.all()

def findItemsByQuery( query ):
	return []
