
from library.db import connectDb, convertToDict

def findCategoryBySanitizedName( sanitizedName ):
	cols = [ 'category_id', 'name', 'sanitized_name', 'description' ]
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( 'SELECT %s FROM category WHERE sanitized_name = %%s' % ', '.join( cols ), sanitizedName )
	category = cursor.fetchone()
	conn.close()
	if category is None: return None
	return dict( zip( cols, category ) )

def listCategories():
	cols = [ 'category_id', 'name', 'sanitized_name' ]
	conn = connectDb()
	cursor = conn.cursor()
	cursor.execute( 'SELECT %s FROM category' % ', '.join( cols ) )
	categories = cursor.fetchall()
	conn.close()
	return convertToDict( categories, cols )


