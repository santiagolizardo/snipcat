
import re

def sanitizeString( string ):
	string = string.lower()
	string = re.sub( '\s+', '-', string )
	string = re.sub( '[^a-z0-9-_]', '', string )
	return string

import logging
from BeautifulSoup import BeautifulSoup, NavigableString

VALID_TAGS = 'strong', 'b', 'i', 'u', 'ul', 'li', 'em', 'br', 'p', 'div'

def stripHtmlTags( html, validTags = VALID_TAGS ):
	try:
		soup = BeautifulSoup( html )
		for tag in soup.findAll( True ):
			if tag.name not in validTags:
				s = ''
				for c in tag.contents:
					if type( c ) != NavigableString:
						c = stripTags( unicode( c ), validTags )
					s += unicode( c )
				tag.replaceWith( s )
		return str( soup )
	except UnicodeDecodeError, ude:
		logging.error( ude )
		return html

if __name__  == '__main__':
	value = '<div><p>Hello <b>t<i>he</i>re</b> my friend!</p></div>'
	print value
	print stripHtmlTags(value, VALID_TAGS)



