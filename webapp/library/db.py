
from google.appengine.api import rdbms

import logging

def convertToDict( rows, cols ): return [ dict( zip( cols, row ) ) for row in rows ]

def connectDb():
	instanceName = 'santiagolizardo:snipcat'
	dbName = 'snipcat_db'
	conn = rdbms.connect( instance = instanceName, database = dbName )
	return conn

