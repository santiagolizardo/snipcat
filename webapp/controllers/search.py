
import webapp2
import urllib
from controllers.base import BaseController
from library.strings import sanitizeString
from library.models.search import SearchQuery, listSearchQueries, findItemsByQuery

class SearchIndexController( BaseController ):

	def get( self ):
		queries = listSearchQueries()
		respValues = {
			'queries': queries
		}
		self.pageConf['title'] = 'Search code snippets and examples ' + self.titleSuffix
		self.renderResponse( 'search/index.html', **respValues )

	def post( self ):
		queryParam = self.request.get( 'query' )
		sanitizedQueryParam = urllib.quote_plus( queryParam )

		searchQuery = SearchQuery(
			string = queryParam,
			sanitized_string = sanitizedQueryParam )
		searchQuery.put()

		self.redirect( webapp2.uri_for( 'search-query', query = sanitizedQueryParam ) )

class SearchQueryController( BaseController ):

	def get( self, query ):
		query = urllib.unquote_plus( query )
		results = findItemsByQuery( query )
		numResults = len( results )

		respValues = {
			'query': query,
			'results': results,
			'numResults': numResults
		}
		self.pageConf['title'] = query + self.titleSuffix
		self.renderResponse( 'search/results.html', **respValues )

