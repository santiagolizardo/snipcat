
from controllers.base import BaseController
from library.models.languages import listLanguages
from library.models.categories import listCategories
from library.models.tags import listPopularTags

class FeedController( BaseController ):

	def get( self ):
		languages = listLanguages()
		categories = listCategories()
		tags = listPopularTags()
		respValues = {
			'languages': languages,
			'categories': categories,
			'tags': tags,
		}
		self.renderResponse( 'index.html', **respValues )


