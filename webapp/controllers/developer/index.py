
import webapp2
from controllers.base import BaseController
from library.models.snips import findSnipsByDeveloperId
from library.models.developers import registerDeveloper, listDevelopers, findDeveloperByUsername, findUrlsByDeveloperId 

class DeveloperIndexController( BaseController ):

	def getCacheKey( self ): return 'developer-index'

	def requestGet( self ):
		developers = listDevelopers()

		respValues = {
			'developers': developers
		}
		self.pageConf['title'] = 'Find developers and take a look at their code' + self.titleSuffix
		return self.renderTemplate( 'developer/index.html', **respValues )

