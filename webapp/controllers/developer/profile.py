
from controllers.base import BaseController
from library.models.developers import listDevelopers, findDeveloperByUsername, findUrlsByDeveloperId
from library.models.snips import findSnipsByDeveloperId

class DeveloperProfileController( BaseController ):

	def get( self, username ):
		developer = findDeveloperByUsername( username )
		if not developer:
			self.response.set_status( 404 )
			self.renderResponse( 'errors/404.html' )
			return
		snippets = findSnipsByDeveloperId( developer['developer_id'] )
		developers = listDevelopers()
		urls = findUrlsByDeveloperId( developer['developer_id'] )

		respValues = {
			'developer': developer,
			'snippets': snippets,
			'developers': developers,
			'urls': urls
		}
		self.pageConf['title'] = '%s developer' % developer['username'] + self.titleSuffix
		self.renderResponse( 'developer/profile.html', **respValues )

