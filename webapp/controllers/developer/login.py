
from library.urls import buildUrl
from controllers.base import BaseController
from library.models.developers import findDeveloperByUsernamePassword

class DeveloperLoginController( BaseController ):

	def get( self ):
		respValues = {
		}
		self.pageConf['title'] = 'Access your code snippets and profile' + self.titleSuffix
		self.renderResponse( 'developer/login.html', **respValues )

	def post( self ):
		username = self.request.get( 'username' )
		password = self.request.get( 'password' )

		developer = findDeveloperByUsernamePassword( username, password )
		import logging
		logging.info(developer)
		if developer:
			self.session['developer'] = developer

		self.redirect( buildUrl( 'index' ) )

