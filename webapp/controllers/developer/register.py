
import webapp2
from controllers.base import BaseController
from library.models.snips import findSnipsByDeveloperId
from library.models.developers import registerDeveloper, listDevelopers, findDeveloperByUsername, findUrlsByDeveloperId 

class DeveloperRegisterController( BaseController ):

	def get( self ):
		respValues = {
		}
		self.pageConf['title'] = 'Create your developer account' + self.titleSuffix
		self.renderResponse( 'developer/register.html', **respValues )

	def post( self ):
		devEmail = self.request.get( 'email' )
		developer = {
			'username': self.request.get( 'username' ),
			'email': devEmail,
			'password': self.request.get( 'password' )
		}
		registerDeveloper( developer )

		from google.appengine.api import mail

		message = mail.EmailMessage(
			sender = "SnipCat.com <bot@snipcat.com>",
			subject = "Your account has been created")

		# if not mail.is_email_valid(to_addr):

		message.to = "John Doe <%s>" % devEmail
		message.body = """
		Thanks for registering!

		Please let us know if you have any questions.

		The SnipCat.com Team
		"""

		message.send()

		self.redirect( webapp2.uri_for( 'dev-index' ) )

