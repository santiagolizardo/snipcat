
from library.urls import buildUrl
from controllers.base import BaseController
from library.models.snips import findSnipsByDeveloperId
from library.models.developers import registerDeveloper, listDevelopers, findDeveloperByUsername, findUrlsByDeveloperId, updateDeveloperUrls

class DeveloperPreferencesController( BaseController ):

	def get( self ):
		username = self.session['developer']['username']
		developer = findDeveloperByUsername( username )
		if not developer:
			self.response.set_status( 404 )
			self.renderResponse( 'errors/404.html' )
			return
		dev_url = findUrlsByDeveloperId( developer['developer_id'] )

		respValues = {
			'developer': developer,
			'devUrl': dev_url
		}
		self.pageConf['title'] = '%s developer' % developer['username'] + self.titleSuffix
		self.renderResponse( 'developer/preferences.html', **respValues )
	
	def post( self ):
		developerId = self.session['developer']['developer_id']
		fields = [ 'ohloh', 'sourceforge', 'github', 'google_code', 'linkedin', 'blog', 'twitter', 'codeplex' ]
		test = {}
		for field in fields:
			test[field] = self.request.get( field )
		updateDeveloperUrls( developerId, test )

		self.redirect( buildUrl( 'dev-preferences' ) )

