
import webapp2
from controllers.base import BaseController
from library.models.snips import findSnipsByDeveloperId
from library.models.developers import registerDeveloper, listDevelopers, findDeveloperByUsername, findUrlsByDeveloperId 

from library.urls import buildUrl

class DeveloperLogoutController( BaseController ):

	def get( self ):
		if 'developer' in self.session:
			del self.session['developer']

		self.redirect( buildUrl( 'index' ) )

