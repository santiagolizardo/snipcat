
from controllers.base import BaseController
from library.models.tags import listPopularTags
from library.models.developers import listPopularDevelopers
from library.models.languages import listPopularLanguages
from library.models.snips import listPopularSnips

class RanksIndexController( BaseController ):

	def get( self ):
		popularSnips = listPopularSnips()
		popularDevelopers = listPopularDevelopers()
		popularTags = listPopularTags()
		popularLanguages = listPopularLanguages()
		respValues = {
			'popularSnips': popularSnips,
			'popularDevelopers': popularDevelopers,
			'popularTags': popularTags,
			'popularLanguages': popularLanguages
		}
		self.pageConf['title'] = 'Ranking of best developers and code' + self.titleSuffix
		self.renderResponse( 'ranks/index.html', **respValues )

