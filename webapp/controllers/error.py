
import logging
from controllers.base import BaseController

def error404( request, response, exception ):
	logging.exception( exception )
	controller = Error404Controller( request, response )
	controller.get()

def error500( request, response, exception ):
	logging.exception( exception )
	controller = Error500Controller( request, response )
	controller.get()

class Error404Controller( BaseController ):

	def get( self ):
		self.response.set_status( 404 )
		self.renderResponse( 'errors/404.html' )

class Error500Controller( BaseController ):

	def get( self ):
		self.response.set_status( 500 )
		self.renderResponse( 'errors/500.html' )

