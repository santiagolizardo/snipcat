

import webapp2
from library.urls import convertFromBaseX, convertToBaseX, buildUrl
from controllers.base import BaseController
from library.models.snips import findSnipById

class EmbedIndexController( BaseController ):

	def get( self, id ):
		id = convertFromBaseX( id )
		snip = findSnipById( id )
		if not snip:
			self.response.set_status( 404 )
			return

		values = {
			'snip': snip,
			'code': snip['code'],
			'hl_code': snip['hl_code']
		}
		self.renderResponse( 'snip/embed.html', **values )

