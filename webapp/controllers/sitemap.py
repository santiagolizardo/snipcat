
from controllers.base import BaseController
from library.models.languages import listLanguages
from library.models.tags import listPopularTags

from datetime import datetime

class SitemapXmlController( BaseController ):

	def getContentType( self ): return 'text/xml; charset=utf-8'
	def getCacheKey( self ): return 'sitemap-xml'

	def requestGet( self ):
		languages = listLanguages()
		tags = listPopularTags()
		respValues = {
			'languages': languages,
			'tags': tags,
			'currentDate': datetime.now().isoformat( '+' )
		}

		output = self.renderTemplate( 'sitemap.xml', **respValues )
		return output

