

import webapp2
from library.urls import convertFromBaseX, convertToBaseX, buildUrl
from controllers.base import BaseController
from library.models.snips import findSnipUrlById

class GoIndexController( BaseController ):

	def get( self, id ):
		id = convertFromBaseX( id )
		snip = findSnipUrlById( id )
		if not snip:
			self.response.set_status( 404 )
			return

		url = buildUrl( 'snip-index', lang = snip['language_name'], cat = snip['category_name'], title = snip['sanitized_title'] )
		self.redirect( url, permanent = True )

