
from controllers.base import BaseController
from library.models.languages import listLanguages
from library.models.categories import listCategories
from library.models.tags import listPopularTags

class IndexController( BaseController ):

	def getCacheKey( self ): return 'index'

	def requestGet( self ):
		languages = listLanguages()
		categories = listCategories()
		tags = listPopularTags()
		respValues = {
			'languages': languages,
			'categories': categories,
			'tags': tags,
		}
		return self.renderTemplate( 'index.html', **respValues )

