
from controllers.base import BaseController
from library.urls import buildUrl
from library.models.languages import listLanguages, findLanguageBySanitizedName
from library.models.categories import listCategories, findCategoryBySanitizedName
from library.models.developers import findDevelopersByLanguageId
from library.models.snips import listSnips

class CodeIndexController( BaseController ):

	def get( self ):
		respValues = {
			'languages': listLanguages(),
			'categories': listCategories()
		}
		self.renderResponse( 'code/index.html', **respValues )

class CodeLanguageController( BaseController ):

	def get( self, lang ):
		language = findLanguageBySanitizedName( lang )
		if language is None:
			self.response.set_status( 404 )
			self.renderResponse( 'errors/404.html' )
			return
		
		self.breadcrumb.append( { 'name': language['name'] } )
		
		respValues = {
			'breadcrumb': self.breadcrumb,
			'language': language,
			'categories': listCategories(),
			'developers': findDevelopersByLanguageId( language['language_id'] )
		}
		self.pageConf['title'] = ( '%(langName)s code examples. %(langName)s developers' + self.titleSuffix ) % { 'langName': language['name'] }
		self.renderResponse( 'code/language.html', **respValues )

class CodeLanguageCategoryController( BaseController ):

	def get( self, lang, cat ):
		language = findLanguageBySanitizedName( lang )
		category = findCategoryBySanitizedName( cat )
		
		if language is None or category is None:
			self.response.set_status( 404 )
			self.renderResponse( 'errors/404.html' )
			return			
		
		snips = listSnips( language['language_id'], category['category_id'] )

		self.breadcrumb.append( { 'name': language['name'], 'link': buildUrl( 'code-lang', lang = language['sanitized_name'] ) } )
		self.breadcrumb.append( { 'name': category['name'] } )

		respValues = {
			'breadcrumb': self.breadcrumb,
			'language': language,
			'category': category,
			'snips': snips
		}
		self.pageConf['title'] = '%s in %s examples' % ( category['name'], language['name'] ) + self.titleSuffix
		self.renderResponse( 'code/category.html', **respValues )

