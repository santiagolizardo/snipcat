
from controllers.base import BaseController
from library.models.snips import addSnip
from library.models.tags import Tag
from library.strings import sanitizeString, stripHtmlTags
from recaptcha.client.captcha import displayhtml, submit
from library.models.languages import listLanguages
from library.models.categories import listCategories

class SnipAddController( BaseController ):

	def get( self ):
		languages = listLanguages()
		categories = listCategories()
		recaptchaPublicKey = self.app.config.get( 'recaptchaPublicKey' )
		respValues = {
			'languages': languages,
			'categories': categories,
			'captchaHtml': displayhtml( recaptchaPublicKey )
		}
		self.pageConf['title'] = 'Add a code snippet to the catalog' + self.titleSuffix
		self.renderResponse( 'snip/add.html', **respValues )

	def post( self ):
		tags = self.request.get( 'tags' ).split( ',' )
		safeTags = []
		tagEntities = []
		for tag in tags:
			tag = tag.strip()
			if tag != '':
				sanitizedTag = sanitizeString( tag )
				safeTags.append( sanitizedTag )
				tagEntities.append( Tag( name = sanitizedTag ) )

		recaptchaPrivateKey = self.app.config.get( 'recaptchaPrivateKey' )

		response = submit(
			self.request.get( 'recaptcha_challenge_field' ),
			self.request.get( 'recaptcha_response_field' ),
			recaptchaPrivateKey,
			self.request.remote_addr )
		if not response.is_valid:
			self.response.out.write( 'bad key' )
			return

		description = self.request.get( 'description' )
		description = stripHtmlTags( description )

		output = self.request.get( 'output' )
		output = stripHtmlTags( output, [] )
		
		snip = {
			'languageId': int( self.request.get( 'languageId' ) ),
			'categoryId': int( self.request.get( 'categoryId' ) ),
			'developerId': int( self.request.get( 'developerId' ) ),
			'code': self.request.get( 'code' ),
			'output': output,
			'title': self.request.get( 'title' ),
			'description': description,
			'tags': ','.join( safeTags )
		}
		addSnip( snip )

		self.redirect_to( 'dev-index' )

