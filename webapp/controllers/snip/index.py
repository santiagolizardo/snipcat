
from library.urls import convertToBaseX, buildUrl
from controllers.base import BaseController
from library.models.snips import findSnipBySanitizedTitle, increaseSnipViews

class SnipIndexController( BaseController ):

	def getCacheKey( self ):
		cacheKey = 'snip-index' + self.request.path.replace( '/', '-' )
		return cacheKey

	def requestGet( self, lang, cat, title ):
		snip = findSnipBySanitizedTitle( title )
		if snip is None:
			self.response.set_status( 404 )
			self.renderResponse( 'errors/404.html' )
			return

		alreadyViewed = False
		if 'snipViews' in self.session:
			alreadyViewed = snip['snip_id'] in self.session['snipViews']
			if not alreadyViewed:
				self.session['snipViews'].append( snip['snip_id'] )
		else:
			self.session['snipViews'] = [ snip['snip_id'] ]

		if not alreadyViewed:
			increaseSnipViews( snip['snip_id'] )


		snip['shortcut_url'] = buildUrl( 'go-index', id = convertToBaseX( snip['snip_id'] ), _full = True )	
		snip['embed_url'] = buildUrl( 'embed-index', id = convertToBaseX( snip['snip_id'] ), _full = True )	

		self.breadcrumb.append( { 'name': snip['language_name'], 'link': buildUrl( 'code-lang', lang = snip['language_sanitized_name'] ) } )
		self.breadcrumb.append( { 'name': snip['category_name'], 'link': buildUrl( 'code-lang-cat', lang = snip['language_sanitized_name'], cat = snip['category_sanitized_name'] ) } )
		self.breadcrumb.append( { 'name': snip['title'] } )

		values = {
			'breadcrumb': self.breadcrumb,
			'snip': snip,
		}
		self.javaScripts.extend([ '/scripts/code_hl/prettify.js', '/scripts/snip_index.js' ])
		self.javaScripts.append( '/scripts/jquery-ui-1.8.16.custom.min.js' )
		self.styleSheets.append( '/scripts/code_hl/prettify.css' )
		self.styleSheets.append( '/styles/blitzer/jquery-ui-1.8.16.custom.css' )
		self.pageConf['title'] = snip['title'] + self.titleSuffix
		self.pageConf['metaDescription'] = snip['title']
		self.pageConf['metaKeywords'] = ','.join( snip['tags'] )
		return self.renderTemplate( 'snip/index.html', **values )

