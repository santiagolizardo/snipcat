
from controllers.base import BaseController
from library.models.snips import castSnipVote

class SnipVoteController( BaseController ):

	def get( self ):
		snipId = int( self.request.get( 'snipId' ) )
		voteValue = int( self.request.get( 'voteValue' ) )
		castSnipVote( snipId, voteValue )

		self.response.headers['Content-type'] = 'text/plain'
		self.response.headers['Cache-Control'] = 'no-cache, must-revalidate'
		self.response.headers['Expires'] = 'Sat, 26 Jul 1997 05:00:00 GMT'
		self.response.out.write( 'OK' )

