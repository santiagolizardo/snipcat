
$( document ).ready( function()
{
	prettyPrint();
	// console.log( $( 'pre.prettyprint' ).html() );

	$( 'ul.vote-links > li > a' ).hover( function()
	{
		$( 'div#voteTooltip' ).html( $( this ).attr( 'title' ) );
	},
	function()
	{
		$( 'div#voteTooltip' ).html('&nbsp;');
	} );

	$( 'ul.vote-links > li > a' ).click( function()
	{
		var snipId = $( 'span#snipId' ).html();
		var voteValue = $( this ).html();
		var url = 'http://snippet' + APP_DOMAIN + '/vote';
		var params = {
			'snipId': snipId,
			'voteValue': voteValue
		};
		$.get( url, params );
		$( 'div#votingArea' ).html( 'Great feedback!' );

		return false;
	} );

	$( document.getElementById( 'embed-link' ) ).click( function()
		{
			$( document.getElementById( 'embed-dialog' ) ).dialog({
				title: 'Embed code snippet',
				position: 'top',
				modal: true,
				width: '80%',
				height: 400,
			});
		}
	);
} );
