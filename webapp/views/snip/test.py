
import string

baseChars = list(  string.digits + string.letters + '_-' )
base = len( baseChars )

def convertToBaseX( number ):
	string = ''
	total = number
	while total:
		remainder = total % base
		string = baseChars[ remainder ] + string
		quotient = total // base
		total = quotient
	return string

def convertToBase10( string ):
	number = 0
	stringLen = len( string )
	i = stringLen - 1
	while i > -1:
		pos = baseChars.index( string[i-1] )
		plus = pos * ( 64 ** i )
		number += plus 
		i -= 1
	return number
		
