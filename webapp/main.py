
import webapp2
from webapp2_extras import routes

import os

debugEnabled = os.environ.get( 'SERVER_SOFTWARE', '' ).startswith( 'Dev' )
appDomain = '.snipcat.dev' if debugEnabled else '.snipcat.com'
appDomainPort = ':8080' if debugEnabled else ''

routes = [
	routes.DomainRoute( 'api' + appDomain, [
		webapp2.Route( '/', handler = 'controllers.api.index.ApiIndexController', name = 'api-index' ),
	]),
	routes.DomainRoute( 'embed' + appDomain, [
		webapp2.Route( '/<id>', handler = 'controllers.embed.EmbedIndexController', name = 'embed-index' ),
	]),
	routes.DomainRoute( 'go' + appDomain, [
		webapp2.Route( '/<id>', handler = 'controllers.go.GoIndexController', name = 'go-index' ),
	]),
	routes.DomainRoute( 'code' + appDomain, [
		webapp2.Route( '/<lang>/<cat>/<title>', handler = 'controllers.snip.index.SnipIndexController', name = 'snip-index' ),
		webapp2.Route( '/<lang>/<cat>', handler = 'controllers.code.CodeLanguageCategoryController', name = 'code-lang-cat' ),
		webapp2.Route( '/<lang>', handler = 'controllers.code.CodeLanguageController', name = 'code-lang' ),
		webapp2.Route( '/', handler = 'controllers.code.CodeIndexController', name = 'code-index' ),
	]),
	routes.DomainRoute( 'search' + appDomain, [
		webapp2.Route( '/<query>', handler = 'controllers.search.SearchQueryController', name = 'search-query' ),
		webapp2.Route( '/', handler = 'controllers.search.SearchIndexController', name = 'search-index' ),
	]),
	routes.DomainRoute( 'developer' + appDomain, [
		webapp2.Route( '/register', handler = 'controllers.developer.register.DeveloperRegisterController', name = 'dev-register' ),
		webapp2.Route( '/login', handler = 'controllers.developer.login.DeveloperLoginController', name = 'dev-login' ),
		webapp2.Route( '/logout', handler = 'controllers.developer.logout.DeveloperLogoutController', name = 'dev-logout' ),
		webapp2.Route( '/preferences', handler = 'controllers.developer.preferences.DeveloperPreferencesController', name = 'dev-preferences' ),
		webapp2.Route( '/<username>/feed', handler = 'controllers.developer.feed.DeveloperFeedController', name = 'dev-feed' ),
		webapp2.Route( '/<username>', handler = 'controllers.developer.profile.DeveloperProfileController', name = 'dev-username' ),
		webapp2.Route( '/', handler = 'controllers.developer.index.DeveloperIndexController', name = 'dev-index' ),
	]),
	routes.DomainRoute( 'ranks' + appDomain, [
		webapp2.Route( '/', handler = 'controllers.ranks.RanksIndexController', name = 'ranks-index' ),
	]),
	routes.DomainRoute( 'snippet' + appDomain, [
		webapp2.Route( '/add', handler = 'controllers.snip.add.SnipAddController', name = 'snippet-add' ),
		webapp2.Route( '/vote', handler = 'controllers.snip.vote.SnipVoteController', name = 'snippet-vote' ),
	]),
	webapp2.Route( '/feed.xml', handler = 'controllers.feed.FeedXmlController', name = 'feed-xml' ),
	webapp2.Route( '/sitemap.xml', handler = 'controllers.sitemap.SitemapXmlController', name = 'sitemap-xml' ),
	webapp2.Route( '/', handler = 'controllers.index.IndexController', name = 'index' ),
]

config = {
	'appDomain': appDomain + appDomainPort,
	'recaptchaPublicKey': '6LdWy8kSAAAAAL5ljBLdur3cJIxqnBgJR1W0r-dC',
	'recaptchaPrivateKey': '6LdWy8kSAAAAAMKralopGEkGXSiA_l8OuI3fGu6R',
}

config['webapp2_extras.sessions'] = {
	'secret_key': 'SnipDogg',
	'cookie_args': { 'domain': appDomain }
}

app = webapp2.WSGIApplication( routes = routes, debug = debugEnabled, config = config)
app.error_handlers[404] = 'controllers.error.error404'
app.error_handlers[405] = 'controllers.error.error405' # Method not allowed
app.error_handlers[500] = 'controllers.error.error500'

def main():
	app.run()

if __name__ == '__main__':
	main()

